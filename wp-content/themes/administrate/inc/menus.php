<?php
register_nav_menus(
    array(
        'header' => __('Header Navigation'),
        'top-menu' => __( 'Top Menu' ),
        'Product menu' => __('Product Menu'),
        'Solutions menu' => __('Solutions Menu'),
        'More menu' => __('More Menu'),
        'About menu' => __('About Menu'),
        'Help menu' => __('Help Menu')
    )
);
/**
 * Used to fill menus with the menu items
 *
 * @param type $menu_id
 * @param type $items
 * @param type $parent_id
 * @param type $status
 */
function add_menu_items ($menu_id, $items, $parent_id = 0) {
    foreach ($items as $item) {
        $menu_item_id = wp_update_nav_menu_item(
            $menu_id,
            0,
            array(
                'menu-item-title'     => __($item[0]),
                'menu-item-url'       => $item[1],
                'menu-item-status'    => 'publish',
                'menu-item-parent-id' => $parent_id
            )
        );
        if(isset($item[2]) && !empty($item[2])) {
            add_menu_items ($menu_id, $item[2], $menu_item_id);
        }
    }
}
/**
 * Initialize menus
 */
function register_menu_items()
{
    //Header menu items
    $headerMenuName = 'Header Menu';
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($headerMenuName);
    if(!$menu_exists):
        $headerMenuId   = wp_create_nav_menu($headerMenuName);
        // Array contains Label , URL, Submenu links array.
        $tour_links = array(
            array("option1", home_url('#')),
            array("options2", home_url('#'))
        );
        $customers_links = array(
            array("option1", home_url('#')),
            array("options2", home_url('#')),
            array("option3", home_url('#')),
            array("options4", home_url('#')),
            array("option5", home_url('#')),
            array("options6", home_url('#'))
        );
        $resources_links = array(
            array("option1", home_url('#')),
            array("options2", home_url('#')),
            array("option3", home_url('#'))
        );
        $header_links = array(
            array("Tour", '#', $tour_links),
            array("Pricing", home_url('#')),
            array("Customers", '#', $customers_links),
            array("Blog", home_url('#')),
            array("Resources", '#', $resources_links),
            array("About", home_url('#')),
            array("Free Trial", home_url('#'))
        );
        add_menu_items($headerMenuId, $header_links);
    else:
        $headerMenuId   = $menu_exists->term_id;
    endif;
    
    
    
    //Top header menu
     $topHeaderMenuName = 'Top Header Menu';
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($topHeaderMenuName);
    if(!$menu_exists):
        $topHeaderMenuId   = wp_create_nav_menu($topHeaderMenuName);
        $contact_links = array(
            array("option1", home_url('#')),
            array("options2", home_url('#'))
        );
        $lang_links = array(
            array("French", home_url('#')),
            array("Arabic", home_url('#'))
        );
        $topHeader_links = array(
            array("Login", home_url('#')),
            array("Support", home_url('#')),
            array("Contact Us", '#', $contact_links),
            array("English (UK)", '#', $lang_links)
        );
        add_menu_items($topHeaderMenuId, $topHeader_links);
    else:
        $topHeaderMenuId   = $menu_exists->term_id;
    endif;

    
    
     //Product menu
     $productsMenuName = 'Products Menu';
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($productsMenuName);
    if(!$menu_exists):
        $productsMenuId   = wp_create_nav_menu($productsMenuName);
        $products_links = array(
            array("Tour", home_url('#')),
            array("Pricing", home_url('#')),
            array("Implementation", home_url('#')),
            array("Integrations", home_url('#')),
            array("Roadmap", home_url('#')),
            array("Trust", home_url('#')),
            array("Status", home_url('#')),
        );
        add_menu_items($productsMenuId, $products_links);
    else:
        $productsMenuId   = $menu_exists->term_id;
    endif;
    
    //Solutions menu
     $solutionsMenuName = 'Solutions Menu';
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($solutionsMenuName);
    if(!$menu_exists):
        $solutionsMenuId   = wp_create_nav_menu($solutionsMenuName);
        $solutions_links = array(
            array("Keep Track", home_url('#')),
            array("Save Time", home_url('#')),
            array("Increase Bookings", home_url('#')),
            array("Exceptional elearning", home_url('#'))
        );
        add_menu_items($solutionsMenuId, $solutions_links);
    else:
        $solutionsMenuId   = $menu_exists->term_id;
    endif;
    
    //More menu
     $moreMenuName = 'More Menu';
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($moreMenuName);
    if(!$menu_exists):
        $moreMenuId   = wp_create_nav_menu($moreMenuName);
        $more_links = array(
            array("Sectors", home_url('#')),
            array("Stories", home_url('#')),
            array("Partners", home_url('#')),
            array("Resources", home_url('#')),
            array("Webinars", home_url('#')),
            array("FAQ", home_url('#')),
            array("Payment", home_url('#')),
        );
        add_menu_items($moreMenuId, $more_links);
    else:
        $moreMenuId   = $menu_exists->term_id;
    endif;
    
    
    //About menu
     $aboutMenuName = 'About Menu';
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($aboutMenuName);
    if(!$menu_exists):
        $aboutMenuId   = wp_create_nav_menu($aboutMenuName);
        $about_links = array(
            array("About Us", home_url('#')),
            array("Our values", home_url('#')),
            array("Our Team", home_url('#')),
            array("Contact Us", home_url('#')),
            array("Jobs", home_url('#')),
            array("Press Kits", home_url('#')),
            array("Blog", home_url('#')),
        );
        add_menu_items($aboutMenuId, $about_links);
    else:
        $aboutMenuId   = $menu_exists->term_id;
    endif;
    
    //Help menu
     $helpMenuName = 'Help Menu';
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($helpMenuName);
    if(!$menu_exists):
        $helpMenuId   = wp_create_nav_menu($helpMenuName);
        $help_links = array(
            array("support", home_url('#')),
            array("Login", home_url('#')),
            array("Terms & Conditions", home_url('#')),
            array("Privacy", home_url('#'))
        );
        add_menu_items($helpMenuId, $help_links);
    else:
        $helpMenuId   = $menu_exists->term_id;
    endif;
}

add_action('init', 'register_menu_items');
