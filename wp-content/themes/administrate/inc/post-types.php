<?php

/**
 * Create custom post types using $postTypes array,
 * containing post type plural and singular names.
 */
function bpn_create_post_types()
{
    $postTypes = array(
        'Pricing Rules' => array(
            'singular'    => 'Pricing Rule',
            'plural'      => 'Pricing Rules',
            'has_archive' => false,
            'queryable'   => false,
        ),
        'Plans' => array(
            'singular'    => 'Plan',
            'plural'      => 'Plans',
            'has_archive' => false,
            'queryable'   => false,
        )
    );

    foreach ($postTypes as $postType => $post) {
        $plural   = $post['plural'];
        $singular = $post['singular'];
        if (isset($post['slug'])) {
            $postSlug = $post['slug'];
        } else {
            $postSlug = str_replace(' ', '-', strtolower($singular));
        }
        $postName = str_replace(' ', '_', strtolower($singular));

        $queryable = true;
        if (isset($post['queryable'])) {
            $queryable = $post['queryable'];
        }
        $labels   = array(
            'name'               => $postType,
            'singular_name'      => $singular,
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New ' . $singular,
            'edit_item'          => 'Edit ' . $singular,
            'new_item'           => 'New ' . $singular,
            'all_items'          => 'All ' . $plural,
            'view_item'          => 'View ' . $singular,
            'search_items'       => 'Search ' . $plural,
            'not_found'          => 'No ' . $plural . ' found',
            'not_found_in_trash' => 'No ' . $plural . ' found in Trash',
            'parent_item_colon'  => '',
            'menu_name'          => $postType,
        );

        if (isset($post['supports'])) {
            $supports = $post['supports'];
        } else {
            $supports = array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'revisions',
                'comments',
            );
        }

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => $queryable,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array(
                'slug'       => $postSlug,
                'with_front' => true
            ),
            'capability_type'    => 'post',
            'has_archive'        => $post['has_archive'],
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => $supports,
            'taxonomies'         => array('post_tag'),
        );

        register_post_type($postName, $args);
    }
}
add_action('init', 'bpn_create_post_types');
