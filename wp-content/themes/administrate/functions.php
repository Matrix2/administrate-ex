<?php
define("LS_TEMPLATE_DIRECTORY", get_template_directory());
/*
 * Custom menus for this theme.
 */
require LS_TEMPLATE_DIRECTORY . '/inc/menus.php';
/*
 * Custom post types for this theme.
 */
require LS_TEMPLATE_DIRECTORY . '/inc/post-types.php';

// wp_register_style() example
wp_register_style(
    'style', // handle name
    get_stylesheet_uri()  // the URL of the stylesheet
);
wp_register_style(
    'bootstrap.min', // handle name
    get_template_directory_uri() . '/css/bootstrap.min.css'  // the URL of the stylesheet
);
wp_register_style(
    'font-awesome', // handle name
    get_template_directory_uri() . '/css/font-awesome/css/font-awesome.css'  // the URL of the stylesheet
);
wp_register_style(
    'selectric', // handle name
    get_template_directory_uri() . '/css/selectric.css'  // the URL of the stylesheet
);
function mytheme_custom_styles() {
    wp_enqueue_style('bootstrap.min');
    wp_enqueue_style('font-awesome');
    wp_enqueue_style( 'style' );
    wp_enqueue_style( 'selectric' );
}
add_action( 'wp_enqueue_scripts', 'mytheme_custom_styles' );
function scripts()
{
    wp_enqueue_script(
        'jquery',
        get_template_directory_uri() . '/js/jquery.min.js',
        false,
        false,
        true
    );
    wp_enqueue_script(
        'bootstrap',
        get_template_directory_uri() . '/js/bootstrap.min.js',
        array('jquery'),
        false,
        true
    );
    wp_enqueue_script(
        'selectric',
        get_template_directory_uri() . '/js/jquery.selectric.min.js',
        array('jquery'),
        false,
        true
    );
    wp_enqueue_script(
        'scripts',
        get_template_directory_uri() . '/js/scripts.js',
        array('jquery','bootstrap', 'selectric'),
        false,
        true
    );

}
add_action('wp_enqueue_scripts', 'scripts');

// Widgets
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Sidebar Widgets',
        'id'   => 'sidebar-widgets',
        'description'   => 'These are widgets for the sidebar.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
}