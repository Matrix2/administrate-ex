if(jQuery("select").length > 0) {
    jQuery("select").selectric({
        disableOnMobile: false,
        responsive: true
    });
}
    jQuery('li').each(function(){
        item = jQuery(this);
        if (item.hasClass('current-menu-parent')){
            item.addClass('dropdown');
            item.children('a').addClass('dropdown-toggle target');
            item.children('a').attr('data-toggle','dropdown');
            item.children('a').attr('aria-haspopup',"true");
            item.children('a').attr('role','button');
            item.children('a').attr('aria-expanded','false');
        }
    });
    jQuery('ul').each(function(){
        item = jQuery(this);
        if (item.hasClass('sub-menu')){
           item.addClass('dropdown-menu');
        }
    });


