<?php get_header(); ?>
    <div class='pricing-options'>
	<div class="main_title">
            <div class="container">
		<h1 class="text-center">Pricing Plans</h1>
                <div class="currency">
                    <div class="currency-title">Currency</div>
                    <select class="currency-dropdown">
                        <option>GBP</option>
                        <option>EUR</option>
                        <option>USD</option>
                    </select>
                </div>
            </div>
	</div>
	<div class="pricing-new-experiment">
            <div class='section panels'>
                <div class='container'>
                    <div class='row wrapper'>
                        <?php 
                        $args = array('post_type' => 'pricing_rule','order' => 'ASC', 'posts_per_page' => 10);
                        $loop = new WP_Query($args);
                        $loop->set('orderby', 'date');
                        $loop->set('order', 'ASC');
                        while ($loop->have_posts()) : 
                            $loop->the_post();
                            include 'templates/page-pricing-rules.php';
                        endwhile;
                        ?>
                    </div>
                </div>
            </div>
            <div class="section-plans">
                <div class="container">
                    <h1 class="text-center">Every Plan Includes</h1>
                    <div class="row text-center wrapper">
                     <?php
                        $args = array('post_type' => 'plan', 'order' => 'ASC', 'posts_per_page' => 10);
                        $loop = new WP_Query($args);
                        $i =0;
                        while ($loop->have_posts()) :
                            $loop->the_post();
                            if($i%2==0):
                                $class= 'col-sm-5 col-sm-offset-1';
                                echo "</div><div class='row text-center wrapper'>";
                            else:
                                $class='col-sm-5';
                            endif;
                             include 'templates/page-plans.php';
                            $i++;
                        endwhile;
                    ?> 
                    </div>
                </div>
            </div>
	</div>
    </div>
<?php get_footer(); ?>