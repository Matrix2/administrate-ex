<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!--[if lt IE 9]>
	<script src="dist/html5shiv.js"></script>
	<![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Responsive Price Plan</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
        <?php wp_head(); ?>
</head>
<body>
    <div class="top_main_header_nav">
	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
                    <div class='top-menu'>
                        <div id="bs-example-navbar-collapse-top" class="menu-header-top-menu-container">
                          <?php wp_nav_menu(array('menu_class'=>'nav navbar-nav navbar-right','theme_location' => 'top-menu')); ?>  
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    <nav class="navbar navbar-default">
	<div class="container">
            <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Administrate</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<?php wp_nav_menu(array('menu_class'=>'nav navbar-nav navbar-right','theme_location' => 'header')); ?>
            </div>
            <!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
    </nav>

