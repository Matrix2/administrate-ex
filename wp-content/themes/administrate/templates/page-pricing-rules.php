<?php
/**
 * Template Name: Pricing Rules
 **/
$adminPrice = get_post_meta($post->ID,'adminPrice',true);
$learnerPrice = get_post_meta($post->ID,'learnerPrice',true);
$iconClass = get_post_meta($post->ID,'iconClass',true);
$includes = get_post_meta($post->ID,'includes',true);
$rule = get_post_meta($post->ID,'rule',true);
$isPopular = get_post_meta($post->ID,'isPopular',true);
$featuresLabel = get_post_meta($post->ID,'featuresLabel',true);
$featuresLink = get_post_meta($post->ID,'featuresLink',true);
$boxNumber = get_post_meta($post->ID,'boxNumber',true);
$isEnterprise = get_post_meta($post->ID,'isEnterprise',true);
?>
<div class="col-md-4 col-sm-6 plus box">
    <div class="panel panel-primary center-block"<?php ($isEnterprise == true)? $id='enterprise' : $id='panel' ; ?> id ="<?php echo $id; ?>" >
        <?php
         if($isEnterprise): ?>
            <div class="panel-heading text-center">
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
            <div class="panel-body <?php echo $boxNumber; ?>">
                <div class="row">
                    <div class="col-xs-12 text-center" style="margin-top: 150px;" >
                        <p><?php echo $rule; ?></p>
                        <br>
                        <br>
                        <div class='include'><?php echo $includes; ?></div>
                        <ul class='col-sm-offset-1'>
                            <?php
                            if (have_rows('features')):
                                // loop through the rows of data
                                while (have_rows('features')) : the_row();
                                    // display a sub field value
                                    ?>
                                    <li>
                                        <span class="checkmark">
                                            <div class="checkmark_circle"></div>
                                            <div class="checkmark_stem"></div>
                                            <div class="checkmark_kick"></div>
                                        </span>
                                        <?php the_sub_field('feature'); ?>
                                    </li>											
                                    <?php
                                endwhile;
                            endif;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a class="btn btn-primary" href="/about/contact-us/">Contact Us</a>
                        <p class="see_all_features">&nbsp;</p>
                    </div>
                </div>
            </div>
        <?php
             else:;
        ?>
        <?php if($isPopular): ?>
        <div class="plus-announcment text-center text-uppercase">
            <small>Most Popular</small>   
        </div>
        <?php endif; ?>
        <div class="panel-heading text-center">
            <div class="icon_img">
                <div class="icon <?php echo $iconClass; ?> icon-primary"></div>
            </div>
            <h2><?php the_title();?></h2>
            <?php the_content(); ?>
        </div>

        <div class="panel-body <?php echo $boxNumber; ?> ">
            <div class="row">
                <div class="col_sm_half">
                    <div class="col-xs-12">
                        <h1 class="pull-right">
                            <sup class="currency_symbol">£</sup><span id="plus_superuser_price"><?php echo $adminPrice; ?></span><small>/month</small>
                            <p class="pull-left">Administrator</p>
                        </h1>
                    </div>
                </div>
                <div class="col_sm_half">
                    <div class="col-xs-12">
                        <h1 class="pull-right">
                            <sup class="currency_symbol">£</sup><span id="plus_learner_price"><?php echo $learnerPrice; ?></span><small>/month</small>
                            <p class="pull-left">Learner</p>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row"><p>&nbsp;</p></div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class='rule'><?php echo $rule; ?></div>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="row">
                <div class="feature_sec">
                    <img src="https://2y5mo62gdufr3ganc332cx3n-wpengine.netdna-ssl.com/wp-content/themes/administrate2014/img/featureimg.png" alt="Features">
                    <span class="feature_title">Features</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class='include'><?php echo $includes; ?></div>
                    <p>&nbsp;</p>
                    <ul>
                        <?php
                        if( have_rows('features')):
                        // loop through the rows of data
                            while ( have_rows('features') ) : the_row();
                        // display a sub field value
                        ?>
                            <li>
                                <span class="checkmark">
                                    <div class="checkmark_circle"></div>
                                    <div class="checkmark_stem"></div>
                                    <div class="checkmark_kick"></div>
                                </span>
                                <?php the_sub_field('feature'); ?>
                            </li>											
                        <?php 
                            endwhile;
                        endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a class="btn btn-success" href="https://freetrial.getadministrate.com/">Get Started</a>
                    <p class="see_all_features">
                        See <a style="border-bottom: 0px solid transparent; font-size: 16px;" href="<?php echo $featuresLink; ?>"><?php echo $featuresLabel; ?></a>
                    </p>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
</div>