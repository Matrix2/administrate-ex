<?php
/**
 * Template Name: Plans
 **/
$iconClass = get_post_meta($post->ID,'iconClass',true)
?>
    <div class="<?php echo $class; ?>">
        <div class="icon <?php echo $iconClass; ?> icon-primary"></div>
        <h5><?php the_title(); ?></h5><?php the_content(); ?>
    </div>
