<div id="pre-footer" class="container-fluid">
            <div class="container">
                <div class="row col-md-11 col-sm-11 col-xs-11 col-md-offset-1">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                    <p class="text-white"><b>Take a free Trial!</b> <br>
                        No long terms contracts. No credit card required.
                    </p>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
			<div class="row col-md-12 col-sm-12 col-xs-12">
                            
                            <button class="col-md-5 col-sm-5 col-xs-5 btn btn-success" onclick="location.href='https://freetrial.getadministrate.com/'">Free trial</button>
                            <button class="col-md-5 col-sm-5 col-xs-5 col-md-offset-1 col-xs-offset-1 btn btn-primary" onclick="location.href='https://www.getadministrate.com/book-a-demo/'">Book a demo</button>
                        </div>
                    </div>
		</div>
            </div>
	</div>
        <footer class="footer">
            <div id="footer">
              <div class="container">
                <div class="row" style="margin:15px 0;">
                  <div class="col-sm-2 col-sm-offset-1">
                    <div id="nav_menu-2">
                      <h4>Product</h4>
                      <div class="menu-footer">
                        <?php wp_nav_menu(array('theme_location' => 'Product menu')); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div id="nav_menu-3">
                      <h4>Solutions</h4>
                      <div class="menu-footer">
                        <?php wp_nav_menu(array('theme_location' => 'Solutions menu')); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div id="nav_menu-6">
                      <h4>More</h4>
                      <div class="menu-footer">
                        <?php wp_nav_menu(array('theme_location' => 'More menu')); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div id="nav_menu-8">
                      <h4>About</h4>
                      <div class="menu-footer">
                        <?php wp_nav_menu(array('theme_location' => 'About menu')); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div id="nav_menu-5">
                      <h4>Help</h4>
                      <div class="menu-footer">
                        <?php wp_nav_menu(array('theme_location' => 'Help menu')); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="copyright">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-12 col-xs-12" style='text-align: center;'>
                      <p>© 2014-2016 Administrate Limited. Registered in Scotland (No. SC333823)</p>
                    </div>
                    <div class="col-sm-12 col-xs-12" style='position: relative;width: 100%;height: 60px;'>
                      <ul class="horizontal-list">
                        <li><a href="#" title="Facebook Logo" class="pull-left v-align h-align"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#" title="Twitter Logo" class="pull-left v-align h-align sm-bump-left"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#" title="Google Plus Logo" class="pull-left v-align h-align sm-bump-left"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#" title="Linkedin Logo" class="pull-left v-align h-align sm-bump-left"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li><a href="#" title="Youtube Logo" class="pull-left v-align h-align sm-bump-left"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <li>
                          <a href="#" title="RSS Logo" class="pull-left v-align h-align sm-bump-left"><i class="fa fa-rss" aria-hidden="true"></i></a>
                        </li>
                      </ul>

                    </div>
                  </div>
                </div>
              </div>
            </div>
        </footer>
        <?php wp_footer(); ?> 
    </body>
</html>