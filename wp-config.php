<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'administrate-test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '74(}wzR-M_~v4qY2^?8hYfqm#4*;ISqzt4.I}&}(7/s%qswjAMTnux]*^al BTQy');
define('SECURE_AUTH_KEY',  'lbb~<#VU_E8#sYc3<qGjYFe}|}~[u07NL,mW|?:zp89$JH2z_-c|3YN?4tkz?X>(');
define('LOGGED_IN_KEY',    '<;L-CpMB(tr1emPB%5n(|1d~;XK|EIyYN{[9^jM|]Hv3:#(seZz.IZZfoVw> X@[');
define('NONCE_KEY',        '%0}aAHdx-#.Bqt|ICOx6/]ElEZX37$+_8kei/}ILhIAd:!Gf)ZH]_4wdY)Pz3~ug');
define('AUTH_SALT',        'D{.4_WXv^ Wm]EP%h 4L(Kvs.Q:3C%QET$:w2VMUsAX`{|rb72WH^9:7GQ-u=7NF');
define('SECURE_AUTH_SALT', 'NXy1EJd;OBF`voB[2x E0Q;&0i|?IaXoB`Ss.dgmV2=1hqwh _G|i+;G! p?Bnwp');
define('LOGGED_IN_SALT',   'mHK-k3q0doyyvPLxyA5:Jjv-yCK|0dDS >OId~9[`990Nf:Bba]kFo/@#nrNUXj:');
define('NONCE_SALT',       'D%]3oDaIxr+;gEjU.dZ@_gzB($$$K#dy01>NbSqu`EGpL6)O|@]p4{J4[Hw[PHjG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
